﻿#!C:\Python36\python.exe

import argparse
import errno
import os

import boto3
# This tries to load simplejson, but falls back to json.
#  But simplejson is required for handling Decimal type
#from boto.compat import json
import simplejson as json

DESCRIPTION = """Dump the contents of one or more DynamoDB tables to the local filesystem.

Each table is dumped into two files:
  - {table_name}.metadata stores the table's name, schema and provisioned
    throughput.
  - {table_name}.data stores the table's actual contents.

Both files are created in the current directory. To write them somewhere else,
use the --out-dir parameter (the target directory will be created if needed).
"""

def get_tags(client, arn):
    tags = client.list_tags_of_resource(ResourceArn=arn)

    return tags['Tags']

def format_secondary_index(index):
    secondary = { 'IndexName': index['IndexName'],
        'KeySchema': index['KeySchema'],
        'Projection': {
            'ProjectionType': index['Projection']['ProjectionType']
        },
        'ProvisionedThroughput': {
            'ReadCapacityUnits': index['ProvisionedThroughput']['ReadCapacityUnits'],
            'WriteCapacityUnits': index['ProvisionedThroughput']['WriteCapacityUnits']
        }
    }

    if 'NonKeyAttributes' in index['Projection']:
        secondary['Projection']['NonKeyAttributes'] = index['Projection']['NonKeyAttributes']

    return secondary


def get_global_secondary_indexes(table):
    indexes = []

    if (table.global_secondary_indexes):
        for index in table.global_secondary_indexes:
            indexes.append(format_secondary_index(index))

    return indexes

def get_local_secondary_indexes(table):
    indexes = []

    if (table.local_secondary_indexes):
        for index in table.local_secondary_indexes:
            indexes.append(format_secondary_index(index))

    return indexes

def format_autoscaling_policy(resource_id, policy):
    return {
            'PolicyName': policy['PolicyName'],
            'ServiceNamespace' : 'dynamodb',
            'ResourceId' : resource_id,
            'ScalableDimension' : policy['ScalableDimension'],
            'PolicyType' : policy['PolicyType'],
            'TargetTrackingScalingPolicyConfiguration' : policy['TargetTrackingScalingPolicyConfiguration']
        }

def format_autoscaling_target(resource_id, target):
    return {
            'ServiceNamespace' : 'dynamodb',
            'ResourceId' : resource_id,
            'ScalableDimension' : target['ScalableDimension'],
            'MinCapacity' : target['MinCapacity'],
            'MaxCapacity' : target['MaxCapacity'],
            'RoleARN' : target['RoleARN']
        }

def get_autoscaling_targets(asClient, resource_id):
    result = []

    targets = asClient.describe_scalable_targets(ServiceNamespace = 'dynamodb', ResourceIds = [resource_id])
    for target in targets['ScalableTargets']:
        result.append( format_autoscaling_target(resource_id, target))

    return result

def get_autoscaling_policies(asClient, resource_id):
    result = []

    policies = asClient.describe_scaling_policies(ServiceNamespace='dynamodb', ResourceId = resource_id)
    for policy in policies['ScalingPolicies']:
        result.append(format_autoscaling_policy(resource_id, policy))

    return result

def get_autoscaling_info(session, table, global_indexes, local_indexes):
    result = { 
        'targets' : [],
        'policies' : []}

    asClient = session.client('application-autoscaling')

    # table policy
    result['targets'].extend(get_autoscaling_targets(asClient, 'table/' + table.name))
    result['policies'].extend(get_autoscaling_policies(asClient, 'table/' + table.name))

    # global index policies
    for index in global_indexes:
        result['targets'].extend(get_autoscaling_targets(asClient, 'table/' + table.name + '/index/' + index['IndexName']))
        result['policies'].extend(get_autoscaling_policies(asClient, 'table/' + table.name + '/index/' + index['IndexName']))

    # local index policies
    for index in local_indexes:
        result['targets'].extend(get_autoscaling_targets(asClient, 'table/' + table.name + '/index/' + index['IndexName']))
        result['policies'].extend(get_autoscaling_policies(asClient, 'table/' + table.name + '/index/' + index['IndexName']))

    return result


def dump_table(session, client, table, out_dir):
    metadata_file = os.path.join(out_dir, "%s.metadata" % table.table_name)
    data_file = os.path.join(out_dir, "%s.data" % table.table_name)

    metadata = {}
    metadata['attribute_definitions'] = table.attribute_definitions
    metadata['key_schema'] = table.key_schema

    metadata['local_secondary_indexes'] = get_local_secondary_indexes(table)
    metadata['global_secondary_indexes'] = get_global_secondary_indexes(table)
    
    throughput = table.provisioned_throughput
    metadata['throughput'] = { "ReadCapacityUnits" : throughput['ReadCapacityUnits'],
                             "WriteCapacityUnits" : throughput['WriteCapacityUnits'] }

    metadata['stream_specification'] = table.stream_specification

    #    metadata['backup']

    # metadata['point_in_time_recovery']

    if (table.sse_description and table.sse_description['Status'] == "ENABLED"):
        metadata['sse_description'] = "ENABLED"
    else:
        metadata['sse_description'] = "DISABLED"
    
    metadata['tags'] = get_tags(client, table.table_arn)

    metadata['autoscaling'] = get_autoscaling_info(session, table, metadata['global_secondary_indexes'], metadata['local_secondary_indexes'])

    metadata['ttl'] = client.describe_time_to_live(TableName=table.name)['TimeToLiveDescription']

    with open(metadata_file, "w") as metadata_fd:
        json.dump(metadata, metadata_fd)

    with open(data_file, "w") as data_fd:
        lastEvalKey = None

        while True:
            if lastEvalKey:
                response = table.scan(ExclusiveStartKey=lastEvalKey)
            else:
                response = table.scan()
            for item in response['Items']:
                # JSON can't serialize sets -- convert those to lists.
                data = {}
                #convert Item to dict
                for k, v in dict(item).items():
                    if isinstance(v, (set, frozenset)):
                        data[k] = list(v)
                    else:
                        data[k] = v
                data_fd.write(json.dumps(data, use_decimal=True))
                data_fd.write("\n")
            
            if not response.get('LastEvaluatedKey'):
                break
            lastEvalKey = response['LastEvaluatedKey']


def dynamodb_dump(tables, out_dir, profileName, region):
    if profileName == '': profileName = None
    
    session = boto3.Session(profile_name=profileName, region_name=region)

    dynamodb = session.resource('dynamodb')
    client = session.client('dynamodb')

    try:
        os.makedirs(out_dir)
    except OSError as e:
        # We don't care if the dir already exists.
        if e.errno != errno.EEXIST:
            raise
    
    for t in tables:
        table = dynamodb.Table(t)
        print('table: ' + table.table_name)
        dump_table(session, client, table, out_dir)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="dynamodb_dump",
        description=DESCRIPTION
    )
    parser.add_argument("--out-dir", default=".")
    parser.add_argument("--profile", default="")
    parser.add_argument("--region", default="us-east-1")
    parser.add_argument("tables", metavar="TABLES", nargs="+")
    
    namespace = parser.parse_args()

    print("Using profile Name: " + namespace.profile)
    dynamodb_dump(namespace.tables, namespace.out_dir, namespace.profile, namespace.region)
    