﻿#!C:\Python36\python.exe

import argparse
import os
import time
import boto3
import simplejson as json
import re

DESCRIPTION = """Load data into one or more DynamoDB tables.

For each table, data is read from two files:
  - {table_name}.metadata for the table's name, schema and provisioned
    throughput (only required if creating the table).
  - {table_name}.data for the table's actual contents.

Both files are searched for in the current directory. To read them from
somewhere else, use the --in-dir parameter.

This program does not wipe the tables prior to loading data. However, any
items present in the data files will overwrite the table's contents.
"""


def _json_iterload(fd):
    for line in fd:
        try:
            # Try to decode a JSON object.
            json_object = json.loads(line.strip(), use_decimal = True)
        except ValueError:
            if eof and buffer.strip():
                # No more lines to load and the buffer contains something other
                # than whitespace: the file is, in fact, malformed.
                raise
            # We couldn't decode a complete JSON object: load more lines.
            continue

        yield json_object


def create_table(metadata_fd):
    """Create a table from a metadata file-like object."""

def file_len(fname):
    i = 0
    with open(fname) as f:
        for i, l in enumerate(f):
            pass
    return i + 1

def should_increase_throughput(file_name):
    if os.path.getsize(file_name) > 3000000:
        return True

    if file_len(file_name) > 3000:
        return True

    return False

def load_table(table, in_fd):
    print('importing data')
    """Load items into a table from a file-like object."""
    count = 0

    with table.batch_writer() as batch:
        for i in _json_iterload(in_fd):
            # Convert lists back to sets.
            data = {}
            for k, v in i.items():
                if isinstance(v, list) and (len(v) > 0) and isinstance(v[0], str):   # needed to convert list of string to stringset type
                    data[k] = set(v)
                else:
                    data[k] = v

            count = count+1
            if count % 100 == 0:
                print(count)
            batch.put_item(Item = data)
        print('imported {0} items'.format(count))

def filter_tags(tags, table_name):
    filtered = []

    for tag in tags:
        if tag['Key'] == 'Name':
            filtered.append({'Key' : 'Name', 'Value' : table_name})
        else:
            filtered.append(tag)

    return filtered

def get_aws_account_id(session):
     return session.client('sts').get_caller_identity()['Account']

def get_updated_policy_name(resource_id, dimension):
    if dimension == "dynamodb:table:ReadCapacityUnits":
        return 'DynamoDBReadCapacityUtilization:' + resource_id

    return 'DynamoDBWriteCapacityUtilization:' + resource_id

def get_updated_resource_id(table_name, old_resource_id):
    resource_id = 'table/' + table_name
    m = re.search('/index/(?P<idx>.*)', old_resource_id)
    if m is not None:
        resource_id = 'table/' + table_name + '/index/' + m.group('idx')

    return resource_id

def update_autoscaling(session, table_name, autoscaling):
    asClient = session.client('application-autoscaling')

    roleARN = 'arn:aws:iam::' + get_aws_account_id(session) + ':role/aws-service-role/dynamodb.application-autoscaling.amazonaws.com/AWSServiceRoleForApplicationAutoScaling_DynamoDBTable'

    for target in autoscaling['targets']:
        resource_id = get_updated_resource_id(table_name, target['ResourceId'])

        asClient.register_scalable_target(
            ServiceNamespace = 'dynamodb',
            ResourceId = resource_id,
            ScalableDimension = target['ScalableDimension'],
            MinCapacity = target['MinCapacity'],
            MaxCapacity = target['MaxCapacity'],
            RoleARN = roleARN
        )

    for policy in autoscaling['policies']:
        resource_id = get_updated_resource_id(table_name, policy['ResourceId'])
        policy_name = get_updated_policy_name(resource_id, policy['ScalableDimension'])

        asClient.put_scaling_policy(
            PolicyName = policy_name,
            ServiceNamespace = 'dynamodb',
            ResourceId = resource_id,
            ScalableDimension = policy['ScalableDimension'],
            PolicyType = policy['PolicyType'],
            TargetTrackingScalingPolicyConfiguration = policy['TargetTrackingScalingPolicyConfiguration']
        )

def dynamodb_load(tables, in_dir, create_tables, profileName, region, encrypt):
    if profileName == '': profileName = None
    session = boto3.Session(profile_name=profileName, region_name=region)

    temp_write_capacity = 1000

    dynamodb = session.resource('dynamodb')
    client = session.client('dynamodb')
    waiter = client.get_waiter('table_exists')
    
    for t in tables:
        metadata_file = os.path.join(in_dir, "%s.metadata" % t)
        data_file = os.path.join(in_dir, "%s.data" % t)
        # For testing: 
        # t += '2'

        # TODO: Temp Increase GSI write throughput with main throughput

        increase_throughput = should_increase_throughput(data_file)
        increased_throughput = False

        if create_tables:
            print ('Creating table ' + t)

            with open(metadata_file) as meta_fd:
                metadata = json.load(meta_fd)

            previous_throughput = metadata['throughput']['WriteCapacityUnits']

            sse = False
            if encrypt == 'true' or ('sse_description' in metadata and metadata['sse_description'] == 'ENABLED'):
                sse = True

            args = {
                'TableName' : t,
                'AttributeDefinitions' : metadata['attribute_definitions'],
                'KeySchema' : metadata['key_schema'],
                'ProvisionedThroughput' : metadata['throughput'],
                'SSESpecification' : { 'Enabled' : sse }
            }
            
            if len(metadata['local_secondary_indexes']) > 0:
                args['LocalSecondaryIndexes'] = metadata['local_secondary_indexes']
            if len(metadata['global_secondary_indexes']) > 0:
                args['GlobalSecondaryIndexes'] = metadata['global_secondary_indexes']

            if increase_throughput and metadata['throughput']['WriteCapacityUnits'] < temp_write_capacity:
                increased_throughput = True
                print('temporarily increasing throughput for large data import')
                args['ProvisionedThroughput']['WriteCapacityUnits'] = temp_write_capacity

            if metadata['stream_specification'] is not None:
                args['StreamSpecification'] = metadata['stream_specification']

            table = dynamodb.create_table(**args)

            print('Waiting for table creation...')
            waiter.wait(TableName=t)
            print('table created')

            print ('setting tags')
            client.tag_resource(
                ResourceArn = table.table_arn,
                Tags= filter_tags(metadata['tags'], table.name)
            )

            # autoscaling
            if len(metadata['autoscaling']) > 0:
                print('updating auto-scaling')
                update_autoscaling(session, table.name, metadata['autoscaling'])

            # ttl
            if metadata['ttl']['TimeToLiveStatus'] == 'ENABLED':
                print('Updating ttl')
                client.update_time_to_live(
                    TableName=table.name,
                    TimeToLiveSpecification={
                        'Enabled': True,
                        'AttributeName': metadata['ttl']['AttributeName']
                    }
                )
        else:
            print ('Importing data to table ' + t)

            table = dynamodb.Table(t)

            previous_throughput = table.provisioned_throughput['WriteCapacityUnits']

            if increase_throughput and previous_throughput < temp_write_capacity:
                increased_throughput = True
                print('temporarily increasing throughput for large data import')

                client.update_table(TableName=table.name, ProvisionedThroughput = {'ReadCapacityUnits': table.provisioned_throughput['ReadCapacityUnits'], 'WriteCapacityUnits' : temp_write_capacity})
                waiter.wait(TableName=t)

        with open(data_file) as in_fd:
            load_table(table, in_fd)

        if increased_throughput:
            print('resetting throughput')
            client.update_table(TableName=table.name, ProvisionedThroughput= {
               'ReadCapacityUnits' : table.provisioned_throughput['ReadCapacityUnits'], 
               'WriteCapacityUnits' : previous_throughput, 
               }
               )
            waiter.wait(TableName=t)

        print('table import complete')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="dynamodb_load",
        description=DESCRIPTION
    )
    parser.add_argument(
        "--create-tables",
        action="store_true",
        help="Create the tables if they don't exist already (without this flag, attempts to load data into non-existing tables fail)."
    )
    parser.add_argument("--in-dir", default=".")
    parser.add_argument("--profile", default="")
    parser.add_argument("--region", default="us-east-1")
    parser.add_argument("tables", metavar="TABLES", nargs="+")
    parser.add_argument("--encrypt", default="false")

    namespace = parser.parse_args()

    print("Using profile Name: " + namespace.profile)
    dynamodb_load(namespace.tables, namespace.in_dir, namespace.create_tables, namespace.profile, namespace.region, namespace.encrypt)

