# dynamodb-backup-restore

Full offline Backup/Restore for AWS DynamoDB tables.

As of May 2018, AWS offers automatic backup of your DynamoDB tables, there doesn't seem to be an easy way to backup your tables completely offline to restore them.

This is useful for cloning tables with different names or different AWS accounts.  

## History
The python [boto](https://github.com/boto/boto3) library comes with scripts which this code was originally based on.  dynamodb_dump.py/dynamodb_load.py
However those scripts are still usign the original boto library with DynamoDB v1 api.  This API did not support many features including JSON formatted fields.

These scripts have been updated to use the latest boto3 including support for most AWS DynamoDB features.

## Backup/Restore Features:
- Command line option to specify AWS region and profile
- All data types (JSON formatted fields supported)
- Global/Secondary indexes
- Capacities
- Stream Specification settings
- Encryption setting
- Tags
- Auto-scaling settings
- Time to live
- Temporarily increases Write throughput if necessary for restoring large data sets

## Currently not supported:
- AWS DynamoDB Backup and point in time recovery settings (settings within AWS for their backup system)


## Requires:  
- Python 3.6
- boto3 package

## Usage:
### Backup
python dynamodb_dump.py [--region <region>] [--profile <profileName>] <table_name>

**region (optional)**: AWS region (ex: us-east-1)

**profile (optional)**: AWS profile saved in local AWS credentials (See boto documentation for details on credentials, access keys, etc)

**table_name**:  Name of the table to backup


### Restore
python dynamodb_dump.py [--region <region>] [--profile <profileName>] [--createTable] [--encrypt true/false] <table_name>

**region (optional)**: AWS region (ex: us-east-1)

**profile (optional)**: AWS profile saved in local AWS credentials (See boto documentation for details on credentials, access keys, etc)

**createTable (optional)**:  Optional option to create a new table.  If the table already exists, only the data will be loaded into the table.
        No settings will be updated without this option.

**encrypt (optional)**:  Setting this to true will encrypt the table regardless of the option stored in the .metadata file for the table during backup.
        This is useful for backing up an unencrypted table, and restoring it as encrypted.

**table_name**:  Name of the table to restore.  This must match the filenames of the .data/.metadata files.  Note you can rename a backed up file
        set (.data and .metadata), and import those as the new table name.  The table name stored inside the .metadata is not used.


## Examples:
### Backup MyTable
```
python dynamodb_dump.py --region us-east-1 --profile myAWSProfile MyTable
```

###Restore MyTable (create new table as encrypted)
```
python dynamodb_load.py --region us-east-1 --profile myAWSProfile --encrypt true MyTable 
```